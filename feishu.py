#!/usr/bin/python3
import requests
import json
import sys
import os
import datetime

url = "https://open.feishu.cn/open-apis/bot/v2/hook/955a1f77-5ebf-43ae-89d0-9de2bca57901" #你复制的webhook地址粘贴进url内


def send_message(message):
    payload_message = {
        "msg_type": "text",
        "content": {
            "text": message
        }
    }
    headers = {
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=json.dumps(payload_message))
    return response


if __name__ == '__main__':
    text = sys.argv[1]
    send_message(text)

~