#!/usr/bin/python3
# _*_coding:utf-8 _*_
import json
import requests
import sys


def send_msg(url, msg):
        headers = {'Content-Type': 'application/json; charset=utf-8'}
        data = {
                "at": {
                        "isAtAll": "true",
                        "atUserIds": ["user001", "user002"],
                        "atMobiles": ["15xxx", "18xxx"]
                },
                # 链接消息
                "link": {
                        "messageUrl": "1",
                        "picUrl": "1",
                        "text": "1",
                        "title": "1"
                },
                # markdown消息
                "markdown": {
                        "text": "1",
                        "title": "1"
                },
                # feedCard消息
                "feedCard": {
                        "links": {
                                "picURL": "1",
                                "messageURL": "1",
                                "title": "1"
                        }
                },
                # 文本消息
                "text": {
                        "content": msg
                },
                "msgtype": "text",
                # actionCard消息
                "actionCard": {
                        "hideAvatar": "1",
                        "btnOrientation": "1",
                        "singleTitle": "1",
                        "btns": [{
                                "actionURL": "1",
                                "title": "1"
                        }],
                        "text": "1",
                        "singleURL": "1",
                        "title": "1"
                }
        }

        r = requests.post(url, data=json.dumps(data), headers=headers)
        print("ok")
        return r.text


if __name__ == '__main__':
        # msg = "zabbix告警"
        msg = "告警" + sys.argv[1] + "\n" + sys.argv[2]
        token = "7ef7db65342f433a321b3a1fa1b50077102530dcc079aa421505952d5ba13248%27"  # 改成自己的token
        # 钉钉机器人的url连接
        url = f'https://oapi.dingtalk.com/robot/send?access_token={token}'
        print(send_msg(url, msg))...