#!/usr/bin/python3
# -*- coding:UTF-8 -*-
# =====================Description=====================
# [Features]: SMTP电子邮件发送
# [Usage]: ./sendmail.py receiver@example.com Subject Message
#          Put it in: /usr/local/zabbix/alertscripts
# =====================================================
import sys
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.header import Header
from email.utils import parseaddr, formataddr

# <----------------------------Configure Start--------------------------->
# STMP Config
# 163smtp服务器
SMTP_Host = "smtp.qq.com"
# smtp端口
SMTP_Port = 465
# 163邮箱账号
SMTP_User = "1668629995@qq.com"
# 163邮箱的账号对应的安全码（此处非密码）
SMTP_Pass = "qtgjkmjodocrddhd"
# Mail Config
Mail_From = "Zabbix Monitor<%s>" % SMTP_User
Mail_To = sys.argv[1]
Mail_Subject = sys.argv[2]
Mail_Body = sys.argv[3]


# <----------------------------Configure  End---------------------------->
# 署名格式化函数
def _format_addr(s):
    name, addr = parseaddr(s)
    return formataddr((Header(name, 'utf-8').encode(), addr))


# 格式化署名和接收人信息
message = MIMEMultipart()
message['From'] = _format_addr(Mail_From)
message['To'] = _format_addr(Mail_To)
message['Subject'] = Header(Mail_Subject, 'utf-8').encode()
message.attach(MIMEText(Mail_Body, 'html', 'utf-8'))

# 发送邮件！
try:
    smtpobj = smtplib.SMTP_SSL(SMTP_Host, SMTP_Port)
    smtpobj.login(SMTP_User, SMTP_Pass)
    smtpobj.sendmail(Mail_From, Mail_To, message.as_string())
    print('邮件发送成功')
    smtpobj.quit()
except smtplib.SMTPException as e:
    print('邮件发送失败，Case:%s' % e)